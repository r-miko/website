#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://r-miko.gitlab.io/'
RELATIVE_URLS = False

# Ensure that the output is in the 'public' directory
OUTPUT_PATH = 'public/'  # This should match what's in `pelicanconf.py`

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Uncomment and set these to use Disqus or Google Analytics
#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = 'UA-88560035-1'

#PIWIK_URL = 'piwik.mms.ai/'
#PIWIK_SITE_ID = '1'

