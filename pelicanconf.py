#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Rebecca G. Miko'
SITENAME = 'Rebecca G. Miko'
SITEURL = ''
RELATIVE_URLS = True

TIMEZONE = 'Europe/Dublin'

DEFAULT_LANG = 'en'

# Output path for generated files
OUTPUT_PATH = 'public/'  # Ensure output is in 'public' directory for GitLab Pages

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = (
    ('Google Scholar', 'https://scholar.google.com/citations?user=iTZpMSwAAAAJ&hl=en'),
    ('ORCID', 'https://orcid.org/0000-0001-8193-9711'),
    ('LinkedIN', 'https://www.linkedin.com/in/rebecca-miko'),
    ('Gitlab', 'https://gitlab.com/r-miko')
)

DEFAULT_PAGINATION = 10

# Static paths will be copied without parsing their contents
STATIC_PATHS = [
    'publications',
    'extra/portrait.jpg'
]

EXTRA_PATH_METADATA = {
    'extra/portrait.jpg': {'path': 'scientist-rebecca-miko.jpg'}
}

# Link structure
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'

ARTICLE_SAVE_AS = '{slug}/index.html'
ARTICLE_URL = '{slug}/'

CATEGORY_URL = '{slug}'
CATEGORY_SAVE_AS = '{slug}/index.html'

TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}/index.html'

AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Path for separate template files, used by, e.g., pelican-bibtex
THEME_TEMPLATES_OVERRIDES = []

# Plugins
PLUGIN_PATHS = ['plugins', '../pelican-plugins']
PLUGINS = ['i18n_subsites']

JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}

TYPOGRIFY = True

PYGMENTS_STYLE = 'murphy'
PYGMENTS_RST_OPTIONS = {'linenos': 'table', 'anchorlinenos': 'true'}

# Plugin settings
PLUGINS.extend([
    'pelican_youtube',
    'sitemap',
    'pelican-bootstrapify',
    'pelican_bib'
])

# Sitemap configuration
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.1,
        'pages': 0.8
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    },
    'exclude': ['tag/']
}

# Pelican Bootstrapify
BOOTSTRAPIFY = {
    'table': ['table', 'table-striped', 'table-hover'],
    'img': ['img-fluid', 'border', 'border-primary'],
    'blockquote': ['blockquote'],
}

# Publications
PUBLICATIONS_SRC = 'content/publications/rebecca-miko.bib'
PUBLICATIONS_SPLIT_BY = 'tags'
PUBLICATIONS_UNTAGGED_TITLE = 'Others'

# Theme settings
import alchemy
THEME = alchemy.path()
THEME_TEMPLATES_OVERRIDES.append('templates/pelican-alchemy')

BOOTSTRAP_CSS = 'https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/yeti/bootstrap.min.css'
SITEIMAGE = 'scientist-rebecca-miko.jpg width=160 height=160'
HIDE_AUTHORS = True

SITESUBTITLE = 'PhD Candidate who enables gas-based navigation in robotics, using computationally efficient neural networks'
FOOTER_LINKS = (
    ('BioMachineLearning', 'https://biomachinelearning.net/'),
    ('TU Dresden', 'https://nano.tu-dresden.de/member/rebecca_miko/'),
    ('Bold Hearts', 'https://robocup.herts.ac.uk')
)

ICONS = (
    ('linkedin', 'https://www.linkedin.com/in/rebecca-miko'),
    ('orcid', 'https://orcid.org/0000-0001-8193-9711'),
    ('google', 'https://scholar.google.com/citations?user=iTZpMSwAAAAJ&hl'),
    ('gitlab', 'https://gitlab.com/r-miko')
)

