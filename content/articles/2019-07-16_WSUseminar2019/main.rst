Seminar Talk at Western Sydney University - MARCS Institute for Brain, Behaviour and Development
#######################################################################################################

:date: 2019-07-10 12:23
:tags: science, seminar
:category: presentation
:slug: wsuseminar2019

I am honored to be invited to present a seminar at the **MARCS Institute for Brain, Behaviour and Development** at Western Sydney University. 

**Seminar Details**
=====================
- **Topic:** Robotic Gas-Based Navigation: Distance Estimation from Temporal Cues Using Neuromorphic Olfaction Sensors
- **Date:** July 16, 2019
- **Location:** MARCS Institute for Brain, Behaviour and Development, Western Sydney University

**Image Exracted from Presentation**
-------------------------------------

.. image:: {attach}image_sniffles2019.png
   :alt: A fully autonomous robot implementing a Braitenburg-style gas-based navigation algorithm. 
   :width: 100%
   :align: center

[`Download image <{attach}image_sniffles2019.png>`_]

**Overview**
=============

In this seminar, I will discuss our research on robotic gas-based navigation, specifically addressing how distance can be estimated from temporal cues detected by neuromorphic olfaction sensors. Our work explores innovative methods for interpreting gas dispersion patterns and translating these insights into actionable data for robotic systems. By leveraging neuromorphic sensor technologies, we aim to enhance the accuracy and efficiency of gas-based navigation in complex environments.

I look forward to sharing our findings and learning from those at the MARCS Institute. This opportunity to discuss our work with leading experts in the field is highly anticipated, and I am eager to contribute to the ongoing discourse on advancing robotic sensory technologies.
