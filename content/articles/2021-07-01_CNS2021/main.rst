30th Annual Computational Neuroscience Meeting CNS*2021 Poster Presentation and Teaser Video
#######################################################################################################

:date: 2021-07-01 14:02
:modified: 2021-07-01 14:49
:tags: science, conference
:category: presentation
:slug: cns2021

Our poster, **Replicating Bursting Neurons that Signal Input Slopes with Izhikevich Neurons**, will be presented at the 30th Annual Computational Neuroscience Meeting: CNS*2021. This year's conference will be held online from July 3rd to July 7th (more details on the `official page <https://www.cnsorg.org/cns-2021>`_). 

**Presentation Details**
=========================

- **Topic:** Replicating Bursting Neurons that Signal Input Slopes with Izhikevich Neurons
- **Date and Time:** Mon., Jul. 5, 2021, 5 p.m. (EDT)
- **Location:** Online - P169

In preparation, I've released a teaser video on my `YouTube channel <https://www.youtube.com/@rebecca1798>`_ that is also available below.

**Teaser Video**
-----------------

.. raw:: html

	<video data-dashjs-player src="{attach}video_CNS2021.mp4" width="100%" height="auto" controls>
		Your browser does not support the video tag.
	</video>

[`Download video <{attach}video_CNS2021.mp4>`_]

**Our Poster**
===============

This research builds on the foundational work by Kepecs et al. (2002), which explored how pyramidal neurons use bursts of high-frequency firing to encode specific features of their inputs. Our aim was to create a more efficient model suitable for neuromorphic hardware by employing a network of **intrinsically bursting (IB) Izhikevich neurons**.

**Key Findings:**
==================
- **Input Signal Simulation**: We simulated a Gaussian white noise input signal (sampling rate 400 Hz, μ = 0.003, σ = 0.005) and filtered it through a 20 Hz Butterworth low-pass filter.
- **Bidirectional Slope Detection**: The filtered signal was injected into an excitatory IB Izhikevich neuron, followed by an inverted signal for inhibitory responses. This allowed us to test whether the neuron could detect slopes in both directions, similar to the Kepecs et al. model.
- **Positive Slope Preference**: Our findings showed that the IB Izhikevich neuron fired most frequently at positive slopes, confirming its capability to detect slopes bidirectionally, particularly when the low-pass filter’s cutoff frequency exceeded 20 Hz.

This work demonstrates that IB Izhikevich neurons can replicate complex bursting behaviors with a simplified model, paving the way for more efficient neuromorphic implementations.

If you’re attending CNS*2021, I invite you to visit my poster session to discuss these findings in detail and explore their implications for computational neuroscience.
