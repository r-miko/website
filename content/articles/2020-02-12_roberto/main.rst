Roberto Braitenburg Vehicle Experiment
#######################################################################################################

:date: 2020-02-12 17:12
:tags: science
:category: phd
:slug: trial14

As part of my PhD, I conducted a proof-of-concept study. The purpose of the study was to assess the performance of a robot that integrated the deadband-sampling event-based processing method with the custom electronic nose (e-nose) and the one-dimensional Braitenberg-style navigation algorithm. Confronting live data in a real-world environment, facing noise-ridden, turbulent-induced signals tested the success of this approach.

This post is a part of the supplementary material for my thesis. If you are interested in seeing more details, I will soon provide a reference to the published manuscript.

In the meantime, details on the processing method and the custom e-nose can be found on this `post <https://r-miko.gitlab.io/website/ecs2019/>`_.

**Braitenberg-style Navigation Algorithm**
====================================================

In summary, the algorithm defines how the speed of the motors is adjusted based on its signal processing state, specifically whether it is experiencing an "ON" or "OFF" event:

- **"ON" Event**: The speed is increased but capped at a maximum limit.
- **"OFF" Event**: The speed is reduced but not allowed to drop below a certain threshold.
- **No Event**: If neither event is occurring, the speed remains constant.

As a result of this algorithm, the robot operates in one-dimensional space, mimicking the principles of a Braitenberg vehicle. In this style of navigation, the robot moves towards detected stimuli, reacting only to locally sourced olfactory signals.


**Trial 14 Video (Example from Proof-of-Concept Study)**
---------------------------------------------------------

.. raw:: html

    <video data-dashjs-player src="{attach}roberto.mp4" width="100%" height="auto" controls>
        Your browser does not support the video tag.
    </video>

[`Download video <{attach}roberto.mp4>`_]
