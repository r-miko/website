Gadient-based Algorithm for Gas-based Path Following
#######################################################################################################

:date: 2023-10-13 14:12
:tags: science
:category: phd
:slug: trial2

As part of my PhD research, I collected ground truth data to validate the spiking neural network (SNN) and to make adjustments when necessary. The robot was operated in teleoperation mode, controlled using a PS4 remote controller. During this process, I manually guided the robot forward, and when the sensors were positioned over the rope, I adjusted the robot’s path by steering it left or right accordingly, acting as a supervisor. This manual guidance provided the ground truth data for the experiment. 

A dedicated ROS node was developed to receive and log the sensor data, the robot’s velocity, and the corresponding timestamp. The robot was given a constant maximum velocity, both linear and angular. This data served as the ground truth, i.e., the correct moments to turn left or right, and could then be compared against the ouput of my SNN. 


A dedicated ROS (Robot Operating System) node was developed to receive and log key parameters such as sensor data, robot velocity, and corresponding timestamps. The data collected during this phase served as the ground truth for determining the correct moments to turn left or right, which were later used for comparison with the output from the SNN.

This post is a part of the supplementary material for my thesis. If you are interested in seeing more details, I will soon provide a reference to the published manuscript.


**Trial 2 Video (Example Footage of Ground Truth Data Collection)**
---------------------------------------------------------------------

.. raw:: html

	<video data-dashjs-player src="{attach}video_CC2019VID_trial_2.mp4" width="100%"  height="auto" controls>
		our browser does not support the video tag.
	</video>

[`Download video <{attach}VID_trial_2.mp4>`_]

