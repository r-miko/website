Winning the NEUROTECH Award at CapoCaccia Cognitive Neuromorphic Engineering Workshop
#######################################################################################################

:date: 2019-05-05 12:06
:modified: 2019-09-11 14:12
:tags: science, workshop, award
:category: award
:slug: cc2019

I'm excited to share that our team won the **NEUROTECH Award 2019** for the best neuromorphic demo at the CapoCaccia Cognitive Neuromorphic Engineering Workshop! This prestigious award recognizes innovative work in neuromorphic engineering, and we’re honored to have been selected.

**Our Winning Demo: Event-Based Olfaction**
============================================

Our project focused on **odour source localization using an event-driven neuromorphic system**. We developed a stereo electronic nose setup capable of detecting and inferring the direction of odour plumes in real time. The system was powered by a spiking neural network (SNN) implemented on SpiNNaker hardware, enabling accurate, event-based processing.

Although we initially aimed for a fully autonomous robotic demo, time constraints led us to pivot to a desktop setup. This approach allowed us to fully demonstrate the system's capabilities, including real-time visual and auditory feedback.

**Demonstration Video**
------------------------

.. raw:: html

	<video data-dashjs-player src="{attach}video_CC2019.mp4" width="100%"  height="auto" controls>
		our browser does not support the video tag.
	</video>

[`Download video <{attach}video_CC2019.mp4>`_]



**Collaboration and Recognition**
===================================

Our team, consisting of researchers from the University of Hertfordshire, Bielefeld University, and TU Dresden, came together with a shared vision. This award underscores the power of collaboration and the unique environment of the CapoCaccia Workshop, where intense teamwork leads to success.

More details about the award and our demo can be found in the official NEUROTECH blog post: `NEUROTECH Best Demo Award CapoCaccia 2019 <https://neurotechai.eu/blog/2020/02/09/neurotech-best-demo-award-capocaccia/>`_.
