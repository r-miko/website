RoboCup 2019 Humanoid Kid Size League
######################################################################################################

:date: 2019-11-24 12:11
:modified: 2021-01-25 14:44
:tags: science, competition
:category: event
:slug: robocup2019

As a new member of `Bold Hearts <https://boldhearts.gitlab.io/>`_, I had the privilege of participating in the RoboCup 2019 Humanoid Kid Size League in Sydney, Australia. The event was an exceptional and rewarding experience, offering a unique opportunity to engage with some of the brightest minds in robotics while collaborating with fellow newcomers. The environment fostered innovation, collaboration, and shared passion.

**Short Summary**
==================

Our team's dedication and resilience were unwavering throughout the competition. Despite facing challenges, the synergy among team members, each contributing their unique strengths, was inspiring. The atmosphere at RoboCup 2019, with global teams showcasing innovative approaches, created a stimulating environment that highlighted the importance of teamwork, creativity, and continuous learning in advancing humanoid robotics.

**Documentary of Our Team**
============================

Our participation was also documented in video clips by the award-winning filmmaker Silvia Schmidt, `@kalanea905 <https://www.youtube.com/channel/UCeq8LfswdIb7ngNEx9vICNw>`_. These clips offer valuable insights into the event and our team's efforts. You can view the playlist on YouTube here: `RoboCup 2019 Playlist by Silvia Schmidt <https://www.youtube.com/playlist?list=PLn0bU-HgqzNZ3F7jBygw1z_6bdDfLm0Af>`_.

**Image of the 2019 Team**
----------------------------

.. image:: {attach}photo_robocup2019team.jpg
   :alt: Robocup 2019 Team Photo
   :width: 80%
   :align: center

[`Download image <{attach}photo_robocup2019team.jpg>`_]

**Recognition**
=================

I extend my sincere gratitude to my colleagues and the organizers who made this event possible. I look forward to future opportunities to contribute to and learn from the global robotics community.
