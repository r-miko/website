Innovative Solution to Odour-Based Navigation in Robotics Using Nano-Material-Based Sensors and SpiNNaker-2 Integration
#########################################################################################################################
:date: 2023-09-07 13:48
:modified: 2023-09-14 15:16
:tags: science, seminar
:category: presentation
:slug: tunanoseminar2023

I am pleased to announce that I will be presenting our work at an internal seminar at **Chair of Materials Science and Nanotechnology, TU Dresden**. 

**Seminar Details**
=====================

- **Topic:** Innovative Solution to Odour-Based Navigation in Robotics Using Nano-Material-Based Sensors and SpiNNaker-2 Integration
- **Date and Time:** Thu., Sept. 14, 2023, 1 p.m.
- **Location:** Online - `Join the Seminar <https://tinyurl.com/nanoSeminar-GA>`_ 

**Image Exracted from Presentation**
-------------------------------------

.. image:: {attach}image_tu2023.png
   :alt: A fully autonomous robot implementing a line following approach to navigate using custom built CNT gas sensors.
   :width: 100%
   :align: center

[`Download image <{attach}image_tu2023.png>`_]

**Overview**
=============

Our study introduces a novel approach to odour-based navigation challenges by integrating advanced nano-material-based sensors with bio-inspired neural networks. Drawing inspiration from insect behaviour, we employ a stereo olfaction technique facilitated by custom-made sensors. The sensor data is initially processed on a Jetson platform, with plans to transition to the SpiNNaker-2 platform. This integrated system is currently implemented on a 4-wheel rover, with future prospects for deployment on a quadruped robot. Our research aims to advance the field of odour-based navigation, offering innovative solutions and contributing significantly to the development of robotics technologies.

I look forward to sharing our findings and engaging in discussions about the potential impacts of this research. For those interested, please join us online through the provided link or it can be accessed `here <https://nano.tu-dresden.de/seminar/2023_09_14_rebecca-miko>`_.
