31st Annual Computational Neuroscience Meeting CNS*2022 Poster Presentation
#######################################################################################################

:date: 2022-07-01 14:56
:tags: science, conference
:category: presentation
:slug: cns2022

Our poster, **Determinants of input amplitude and slope detection in bursting neurons**, will be presented at the 31st Annual Computational Neuroscience Meeting: CNS*2022. This year's conference will be held in Melbourne, Australia, from July 16th to July 20th (more details on the `official page <https://www.cnsorg.org/cns-2022>`_). Unfortunately, I will not be able to give the presentation in person. However, I have included a QR code directly on my poster, which will take you to a Zoom room. Here, I would be happy to discuss our work and answer any questions.

**Presentation Details**
=========================

- **Topic:** Determinants of input amplitude and slope detection in bursting neurons
- **Date and Time:** Mon., Jul. 18, 2022, 5:50 p.m. (AEST)
- **Location:** Melbourne, Australia

**Image Extracted from Poster**
--------------------------------

.. image:: {attach}image_CNS2022.png
   :alt: 1 s of 600 s Gaussian white noise (µ = 0.003, σ = 0.005, sampling rate fs =400 Hz). The Butterworth low-pass filter was applied with cutoff frequency fc =35 Hz and then rectified. This was injected into each neuron in the parameter space. The spike train shows the result of a slope detector.
   :width: 100%
   :align: center

[`Download image <{attach}image_CNS2022.png>`_]

**Our Poster**
===============

Our study investigates whether bursting neurons, modeled with Izhikevich differential equations, can detect the amplitudes and slopes of naturalistic input signals. The experiments involve injecting a Gaussian white noise signal with a 25 Hz Butterworth low-pass filter directly into each neuron and recording the bursts, defined as having <10ms intervals between spikes. We analyzed the burst lengths to understand the distribution of burst density over the amplitudes and slopes in the signal. The results were evaluated using receiver operator characteristic (ROC) curves, with the area under the curve (AUC) serving as the fitness function.

**Key Findings:**
==================

- **Slope and Amplitude Detection:** Depending on the parameters, the neurons were capable of detecting either the slope of the signal, the amplitude, or both. This indicates a potential for these neurons to represent different aspects of the input signal in a nuanced way.
- **Simulation Success:** The neurons were successful within the simulation when given a generated naturalistic input signal, demonstrating their ability to replicate the behavior of biological neurons in response to complex stimuli.
- **Future Directions:** We plan to extend this research by investigating how these neurons perform with input signals recorded directly from a neuromorphic sensor (e-nose) in a real-world environment. This will involve dealing with challenges such as wind conditions, temperature variations, and sensor time lags.

If you’re attending CNS*2022, I invite you to visit my poster session and scan the QR code to discuss these findings in detail and explore their implications for computational neuroscience.
