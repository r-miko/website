Awards
########
:lang: en

This page highlights the awards and fellowships I have received in recognition of my work.

**2019 Best Domonstration NEUROTECH Award**
=============================================

I'm excited to share that our team won the **NEUROTECH Award 2019** for the best neuromorphic demo at the CapoCaccia Cognitive Neuromorphic Engineering Workshop! This prestigious award recognizes innovative work in neuromorphic engineering, and we’re honored to have been selected.

See this `cc2019 post <https://r-miko.gitlab.io/website/cc2019/>`_ for more details. 


**2019 Best Poster ECS Award**
================================

I’m excited to share that I won the **Best Poster Award 2019** at the Engineering and Computer Science Conference for my work on **brain-inspired spiking neural networks for gas-based navigation**. This project is a significant step towards enhancing robotic navigation through neural networks and processes inspired by mammalian brains.

See this `ecs2019 post <https://r-miko.gitlab.io/website/ecs2019/>`_ for more details. 

