Publications
############
:template: publications
:lang: en

This page shows peer-reviewed publications. See also my external research profiles, e.g., `ORCID <https://orcid.org/0000-0001-8193-9711>`_ and `Google Scholar <https://scholar.google.de/citations?user=iTZpMSwAAAAJ&hl>`_.